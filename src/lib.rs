use proc_macro::TokenStream;
use quote::{quote, ToTokens};
use syn::{parse_macro_input, parse_quote};

#[proc_macro_attribute]
pub fn testpub(_attr: TokenStream, item: TokenStream) -> TokenStream {
  let mut for_test = parse_macro_input!(item as syn::Item);
  let regular = for_test.clone();

  let public = parse_quote! { pub };
  match for_test {
    syn::Item::Const(ref mut v) => v.vis = public,
    syn::Item::Enum(ref mut v) => v.vis = public,
    syn::Item::ExternCrate(ref mut v) => v.vis = public,
    syn::Item::Fn(ref mut v) => v.vis = public,
    syn::Item::Macro2(ref mut v) => v.vis = public,
    syn::Item::Mod(ref mut v) => v.vis = public,
    syn::Item::Static(ref mut v) => v.vis = public,
    syn::Item::Struct(ref mut v) => v.vis = public,
    syn::Item::Trait(ref mut v) => v.vis = public,
    syn::Item::TraitAlias(ref mut v) => v.vis = public,
    syn::Item::Type(ref mut v) => v.vis = public,
    syn::Item::Union(ref mut v) => v.vis = public,
    syn::Item::Use(ref mut v) => v.vis = public,
    _ => panic!("Unsupported item for testpub: {:?}", for_test.to_token_stream())
  }

  (quote! {
    #[cfg(not(test))]
    #regular
    #[cfg(test)]
    #for_test
  }).into()
}

mod test {
  #[test]
  fn run_ui_tests() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests/ui/*.rs");
  }
}
fn foo()
{
  assert!(bar::b);
}

mod bar {
  use testpub::testpub;

  #[testpub]
  const b: bool = true;
}
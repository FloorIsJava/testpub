#[test]
fn test() {
  assert!(foo::BAR);
}

mod foo {
  use testpub::testpub;

  #[testpub]
  const BAR: bool = true;
}